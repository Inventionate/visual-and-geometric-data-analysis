---
title: "MCA and Cluster Analysis"
author: "Fabian Mundt"
date: "5/07/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(tidyverse)
library(FactoMineR)
library(factoextra)

extract_hight_ctr_cat <- function(axis, criterion) {
    hight_ctr_cat <-
        # This is bad code, try to make it more universal.
        res_mca$var$contrib %>%
        as.data.frame() %>%
        rownames_to_column() %>%
        as_tibble() %>%
        select(
            category = rowname,
            dim = str_glue("Dim {axis}")
        ) %>%
        filter(dim > criterion) %>%
        pull(category)

    hight_ctr_cat
}

modified_rates <- function(mca_res) {
  
  Q <- ncol(mca_res$call$X)
  
  seuil <- 1/Q
  
  e <- data.frame(mca_res$eig)[[1]][data.frame(mca_res$eig)[[1]] >= seuil]
  
  pseudo <- (Q/(Q - 1) * (e - seuil))^2
  
  mrate <- round(pseudo/sum(pseudo) * 100, 2)
  
  cum_mrate <- cumsum(mrate)

  tibble(mod_rates = mrate, cum_mod_rates = cum_mrate)
}
```

## Load Data

```{r import-data}
# We load a whole MCA result object.
# The best thing to do is to load your own analysis results here!
res_mca <- read_rds("Data/mca-result.rds")
```

## Check eigenvalues

We'll just see if everything went well and what the quality of our results is like.

```{r mca-eigen}
fviz_eig(res_mca)

modified_rates(res_mca)
```

## Start interpretation per axis

Well, we've already done this roughly and I'm sure you've improved on that. 
This is about interpreting the results of the MCA. The best way is axis by axis.

## Let's go where no man has gone before (almost)

```{r mca-ca-integration}
# Apply Hierarchical Cluster Analysis on the results of our MCA.
res_hcpc <- HCPC(res_mca)
```

We have actually arrived at the 2.5th dimension!

```{r mca-ca-integration}
# Apply Hierarchical Cluster Analysis on the results of our MCA.

plot(res_hcpc, choice = "3D.map")
```

But what does this map tell us?
