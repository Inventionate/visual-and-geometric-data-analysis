---
title: "Visual and Geometric <br> Data Analysis"
subtitle: "Session 3-1: ggplot2"
author: "Fabian Mundt"
date: 19.03.2020
output:
  xaringan::moon_reader:
    lib_dir: presentation/libs
    chakra: presentation/libs/remark-latest.min.js
    css:
        - presentation/inventionate.css
        - presentation/inventionate-fonts.css
        - presentation/libs/animate.css
    nature:
      ratio: "16:9"
      highlightStyle: tomorrow
      highlightLines: true
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      countIncrementalSlides: false
---

class: center, middle, animated, fadeIn

# .huge[What have we learned?]

--

## .large[&#10122; TIMSS … is a good playground]

--

## .large[&#10123; the tidyverse … is something new]

--

## .large[&#10124; dplyr … is powerful]

--

## .large[&#10125; ggplot2 … is (maybe) awesome]

---

class: animated, fadeIn

# Seminar Plan

.pull-left[

1. .bleach[Infos and Basics]
2. .bleach[Exploratory Data Analysis]
3. **Geometry of a Cloud of Points**
4. .bleach[**Principal Component Analysis**]
5. .bleach[Factor Analysis]
6. .bleach[**Multiple Correspondence Analysis**]
7. .bleach[**Cluster Analysis**]
8. .bleach[Overall Recap]

]

.pull-right.opacity-50[

![© xkcd](presentation/calendar_of_meaningful_dates_2x.png)

]

---

class: center, middle, animated, fadeIn

# .huge[What will we learn today?]

--

## .large[Digging deeper in ggplot2]

(Slides by Thomas Lin Pedersen, ggplot2 Core Developer)

---

class: center, middle, animated, fadeIn

background-image: url("presentation/ggplot-slides/ggplot2-1.png")
background-position: center
background-size: contain

---

class: center, middle, animated, fadeIn

background-image: url("presentation/ggplot-slides/ggplot2-2.png")
background-position: center
background-size: contain

---

class: center, middle, animated, fadeIn

background-image: url("presentation/ggplot-slides/ggplot2-3.png")
background-position: center
background-size: contain

---

class: center, middle, animated, fadeIn

background-image: url("presentation/ggplot-slides/ggplot2-4.png")
background-position: center
background-size: contain

---

class: center, middle, animated, fadeIn

background-image: url("presentation/ggplot-slides/ggplot2-5.png")
background-position: center
background-size: contain
