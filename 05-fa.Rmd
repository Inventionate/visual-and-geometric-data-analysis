---
title: "Visual and Geometric <br> Data Analysis"
subtitle: "Session 5: Factor Analysis"
author: "Fabian Mundt"
date: 02.04.2020
output:
  xaringan::moon_reader:
    lib_dir: presentation/libs
    chakra: presentation/libs/remark-latest.min.js
    css:
        - presentation/inventionate.css
        - presentation/inventionate-fonts.css
        - presentation/libs/animate.css
    nature:
      ratio: "16:9"
      highlightStyle: tomorrow
      highlightLines: true
      slideNumberFormat: |
        <div class="progress-bar-container">
          <div class="progress-bar" style="width: calc(%current% / %total% * 100%);">
          </div>
        </div>
      countIncrementalSlides: false
---

class: animated, fadeIn

# Seminar Plan

.pull-left[

1. .bleach[Infos and Basics]
2. .bleach[Exploratory Data Analysis]
3. .bleach[**Geometry of a Cloud of Points**]
4. .bleach[**Principal Component Analysis**]
5. Factor Analysis
6. .bleach[**Multiple Correspondence Analysis**]
7. .bleach[**Cluster Analysis**]
8. .bleach[Overall Recap]

]

.pull-right.opacity-50[

![© xkcd](presentation/calendar_of_meaningful_dates_2x.png)

]

---


class: center, middle, animated, fadeIn

# .huge[What have we learned?]

--

## .large[&#10122; PCA … uses numeric data]

--

## .large[&#10123; PCA … searchs for components/dimensions/axes]

--

## .large[&#10124; ind and var clouds … are a powerful toolkit]

--

## .large[&#10125; FactoMineR … creates ggplot2 objects]

---

class: center, middle, animated, fadeIn

# .huge[What will we learn today?]

## .large[→ Factor Analysis in Practice]

## .large[→ Factor Analysis vs PCA]

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-1.png)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-2.png)

---

class: center, middle, animated, fadeIn

> Philosophically, components in PCA are weighted composites of observed variables 
> while in the factor model, variables are weighted composites of the factors. 
>
> —psych package (?psych::principal)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-3.png)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-4.png)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-5.png)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-6.png)

---

class: center, middle, animated, fadeIn

![© FM](presentation/cop-fa-7.png)

---

class: center, middle, animated, fadeIn

# .large[&#9874;<br>We will **now** create an R Markdown document together]

```{r, eval=FALSE}
library(FactoMineR)
library(psych)
```
